%% COLLECTIVE NEURONAL RESPONSE TO TWO DISTRIBUTED CURRENT STIMULI (collective anti-phase behavior) 
% leading to a doubled dominant frequency in the summed signal

% SETTING
% clear, seed = 1;
% seed = seed + 1
seed = 36; % seed for the examples in the paper: IPS 71, APS 36
rng(seed) % seed
Path = [pwd, '\IN_sims\'];
clrs = ["b","r","g","k","c","y"];   % colors
per_clr = [0.9290,0.6940,0.1250];   % periodogram color

% TIME
T = 150;            % time span
dt = 0.01;

% COUPLING
n = [40,40];        % number of neurons by cluster
k = length(n);      % number of clusters
N = sum(n);         % total number of neurons

% gap-junctional coupling
eps1 = 0.8;
eps2 = 0.08;
epsilon = [ eps1, eps2; ...
            eps2, eps1 ];
K = getK(n,epsilon);
K = K/N;

% MODEL PARAMETERS
gNa_0 = 30; gK_0 = 20; gL_0 = 0.1; ENa_0 = 45; EK_0 = -80; EL_0 = -60;

sigma_C = 0.03;
mean_C = [1,1];
C_0 = [ randtn(mean_C(1),sigma_C,3*sigma_C,n(1));
        randtn(mean_C(2),sigma_C,3*sigma_C,n(2)) ];

% EXTERNAL CURRENT
I_0 = repelem([-2,-2],n)';      % baseline level of I_ext
A_i = repelem([24,24],n)';      % amplitudes of the first input
input_range = [50,80];

% period for I_0+A
load('data_IN_period.mat')
par = data.par;
period = data.period;
period_Iext = interp1(par,period,I_0(1)+A_i(1),'spline');

% rho (relative propagation delay for the second subpopulation)
% rho_2 = 0.25;   % CIPS 
rho_2 = 0.5;    % CAPS
sigma_rho = 0.2;
mean_rho = [0;rho_2];

% delta (propagation delay)
delta = period_Iext*[randtn(mean_rho(1),sigma_rho,3*sigma_rho,n(1)); randtn(mean_rho(2),sigma_rho,3*sigma_rho,n(2))];
input_range_new = input_range+delta;

% constant stimulus
I = @(t) I_0 + A_i.*(t >= input_range_new(:,1) & t <= input_range_new(:,2));
% for the mean depicted in the figure
ind = 1+[0,cumsum(n(1:end-1))];     % indices of the first neuron in each subpopulation
input_range_2 = input_range+period_Iext*mean_rho;
I_2 = @(t) I_0(ind) + A_i(ind).*(t >= input_range_2(:,1) & t <= input_range_2(:,2));

% INITIAL CONDITIONS (V,h,n,s)
x_0 = [ repelem(-79.91, N), ...
        repelem(0.996, N), ...
        repelem(0.041, N) ];    % near the RS for I_ext = -2

% SIMULATION PARAMETERS
NN = 1;             % distinc parameter noise between neurons (N) or not (1)
shift = 40;         % transient period
sigma_noise = 1;    % std of the input current noise
sigma_par = 0;      % std of parameters' multiplicative noise N(0,sigma_par^2)

params = struct('gNa',gNa_0,'gK',gK_0,'gL',gL_0,'ENa',ENa_0, ...
    'EK',EK_0,'EL',EL_0,'I',I,'C',C_0);
data = struct('model','IN','params',params,'T',T,'dt',dt,'shift',shift, ...
    'n',n,'N',N,'K',K,'epsilon',epsilon,'sigma_par',sigma_par,...
    'sigma_noise',sigma_noise,'sigma_C',sigma_C,'mean_C',mean_C,'heter', ...
    (NN==N),'x_0',x_0,'sigma_rho',sigma_rho,'mean_rho',mean_rho);

% SIMULATION
if data.sigma_par ~= 0
    fn = fieldnames(data.params);
    fn = setdiff(fn,{'C','I'}); % C is not supposed to be random, I is a function
    for k = 1:numel(fn)
        data.params = setfield(data.params,fn{k},getfield(data.params,fn{k})*(1+data.sigma_par*randn(NN,1)));
    end
end

[t,x,ie] = simInterneuron(K,data.params,x_0,data.T,data.dt,data.shift,data.sigma_noise);
if ~isempty(ie), error('The orbit diverged!'), end
data.x = x;
data.t = t;
data.input = I_2(t+shift)';

% PLOTTING
% signals and periodograms
ind = 1+[0,cumsum(n)];          % indices for cluster distinction
int_selected = input_range-shift+mean_rho(2)*period_Iext+[period_Iext,0];%-[0,10];      % selected interval
idx = (t >= int_selected(1)) & (t <= int_selected(2));
x_sel = x(idx,ind(1:end-1));   % the first cell of each (synchronized) cluster

[P_smoothed,f] = periodogram(detrend(x_sel),[],2^18,1000/dt);
[P_i_max,P_i_max_ind] = max(P_smoothed(:,1:length(ind)-1));
[~,P_max_ind] = max(P_i_max);
ind_max = P_i_max_ind(P_max_ind);
P_sel_smoothed = P_smoothed(:,P_max_ind);
P_sel_max = P_sel_smoothed(ind_max); 
f_sel_max = f(ind_max); 

% frequency of the composed signal
composed_signal = sum(x(:,1:N),2);
[P_comp_smoothed,f_comp] = periodogram(detrend(composed_signal(idx)),[],2^18,1000/dt);
[P_comp_max,P_comp_max_ind] = max(P_comp_smoothed);
f_comp_max = f_comp(P_comp_max_ind);

[f_sel_max, f_comp_max, f_comp_max/f_sel_max]

pgram = struct('p_sel',P_smoothed,'f_sel',f,'p_comp',P_comp_smoothed,'f_comp',f_comp,'int_selected',int_selected);
data.pgram = pgram;
data.comp_signal = composed_signal;

file = 'data_IN2x40_ex_CAPS.mat'; newfile = file;
for i = 1:length(dir(Path))-2, if isfile([Path,newfile,'.mat']), newfile = [file,'_',num2str(i)]; else, break; end, end
% save([pwd,'\IN_sims\',newfile,'.mat'],'data');

%load([pwd,'\IN_sims\data_IN2x40_ex_CAPS.mat'])

fig = figure; tlo = tiledlayout(10,3,'TileSpacing','Compact','Padding','tight');
set(fig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[10,8])
f_max_tick = 1400; x_ticks = 0:0.35*1000:f_max_tick;
k = length(data.n);
ind = 1+[0,cumsum(data.n)];
lwd = 0.2;
lwd2 = 0.5;
t = data.t;
int_selected = [0,70];
visible = 'on';
clr = [0 0.8 0];
N_hist = 10^7;

% V_i
nexttile([3,3]), hold on, for i = 1:k, plot(data.t,data.x(:,ind(i):ind(i+1)-1),'color',clrs(i),'LineWidth',lwd); end, set(gca,'box','off')
xlabel('t [ms]'), ylabel('V_i [mV]'), xlim(int_selected), ylim([-100,50]), xticks(int_selected(1):10:int_selected(2))
set(gca,'box','off','Visible',visible)
% plot(data.pgram.int_selected(1)*[1,1],100*[-1,1],'k:'), plot(data.pgram.int_selected(2)*[1,1],[-100,100],'k:'), hold off

% V_comp
ax1 = nexttile([3,3]); hold on, plot(data.t,data.comp_signal,'Color',clr,'LineWidth',lwd2)
plot(data.pgram.int_selected(1)*[1,1],10000*[-1,1],'k:'), plot(data.pgram.int_selected(2)*[1,1],10000*[-1,1],'k:'), hold off
xlabel('t [ms]'), ylabel('V [mV]'), xlim(int_selected), xticks(int_selected(1):10:int_selected(2)), yticks(-7000:3500:3500), ylim([-7000,3500])
set(gca,'box','off','Visible',visible), ax1.YAxis.Exponent = 3;

% inputs
nexttile([2,3]), hold on, for i = 1:k, plot(data.t,data.input(:,i),'color',clrs(i),'LineWidth',lwd2); end, hold off
xlabel('t [ms]'), ylabel('I_{ext,i}(t)'), xlim(int_selected), ylim([-10,30]), yticks([-2,22])
set(gca,'box','off','Visible',visible)

% periodograms
ax2 = nexttile([2,1]); hold on,
yyaxis left, plot(data.pgram.f_sel,data.pgram.p_sel(:,1),'b-','LineWidth',lwd2), ax2.YColor = 'b';
plot(data.pgram.f_sel,data.pgram.p_sel(:,2),'r-','LineWidth',lwd2), ylabel('|P_{1}(f)|, |P_{41}(f)|'), %ylabel('|P_{i}(f)|')
% ylim([0,18]), yticks(0:6:18) % IPS
ylim([0,15]), yticks(0:5:15) % APS
yyaxis right, plot(data.pgram.f_comp,data.pgram.p_comp,'Color',clr,'LineWidth',lwd2), ax2.YColor = clr; ylabel('|P(f)|')
% ax2.YAxis(2).Exponent = 4; ylim([0,60000]), yticks(0:20000:60000) % IPS
ax2.YAxis(2).Exponent = 3; ylim([0,3500]), yticks(0:1000:3000) % APS
xlim([0,f_max_tick]), xticks(x_ticks), xlabel('f [Hz]'), set(gca,'box','off'), hold off

% rho_i
nexttile([2,1]), hold on
rho1 = randtn(data.mean_rho(1),data.sigma_rho,3*data.sigma_rho,N_hist);
rho2 = randtn(data.mean_rho(2),data.sigma_rho,3*data.sigma_rho,N_hist);
hold on, histogram(rho1,10^3,'FaceColor','b','EdgeColor','none','Normalization','pdf')
histogram(rho2,10^3,'FaceColor','r','EdgeColor','none','Normalization','pdf')
xlabel('rho_i'), ylabel('Density'), set(gca,'box','off'), xlim([-0.75,1.25]), ylim([0,2.1]), yticks(0:0.7:2.1), hold off
% xticks([-0.6,-0.35,0,0.25,0.6,0.85]) % IPS
xticks([-0.6,-0.1,0,0.5,0.6,1.1]) % APS
legend('P_1','P_2','location','northeast','Interpreter','latex'), legend boxoff

% C_i
nexttile([2,1]), hold on
C1 = randtn(data.mean_C(1),data.sigma_C,3*data.sigma_C,N_hist);
C2 = randtn(data.mean_C(2),data.sigma_C,3*data.sigma_C,N_hist);
hold on, histogram(C1,10^3,'FaceColor','b','EdgeColor','none','Normalization','pdf')
histogram(C2,10^3,'FaceColor','r','EdgeColor','none','Normalization','pdf')
xlabel('C_i'), ylabel('Density'), xlim([0.9,1.1]), xticks(0.91:0.03:1.09), ylim([0,15]), yticks(0:5:15), set(gca,'box','off'), hold off
legend('P_1','P_2','location','northeast','Interpreter','latex'), legend boxoff

