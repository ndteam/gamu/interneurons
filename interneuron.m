function x_dot = interneuron(x,K,gNa,gK,gL,C,ENa,EK,EL,I)
    % SOURCE: https://doi.org/10.1023/A:1008841325921
    N = length(x)/3;
    
    V = x(1:N);
    h = x(N+1:2*N);
    n = x(2*N+1:3*N);
    
    m = 1 ./ (1+exp(-0.08*(V+26)));
    hinf = 1 ./ (1+exp(0.13*(V+38)));
    tauh = 0.6 ./ (1+exp(-0.12*(V+67)));
    ninf = 1 ./ (1+exp(-0.045*(V+10)));
    taun = 0.5+2 ./ (1+exp(0.045*(V-50)));
    
    INa = gNa.*m.^3.*h.*(V-ENa);
    IL = gL.*(V-EL);
    IK = gK.*n.^4.*(V-EK);
    Iel = sum(K.*(V-V'),2);         % gap-junctional coupling

    x_dot = [(I-IL-INa-IK-Iel) ./ C;
        (hinf-h) ./ tauh;                             
        (ninf-n) ./ taun ];
end
