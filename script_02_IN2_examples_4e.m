%% TWO ELECTRICALLY COUPLED INTERNEURONS (RS)
% examples of various dynamics in the model of two coupled INs

% % clear, seed = 13;
% seed = seed+1;
% rng(seed);
Path = [pwd, '\IN_sims\'];
clrs = ["b","r","g","k","c","y"];   % colors
per_clr = [0.9290,0.6940,0.1250];   % periodogram color

% TIME
T = 1000;       % time span
dt = 0.01;

% COUPLING
epsilon = 0.04*2;
n = 2;
N = sum(n);
K = getK(n,epsilon);
K = K/N;

% MODEL PARAMETERS
gNa_0 = 30; gK_0 = 20; gL_0 = 0.1;
ENa_0 = 45; EK_0 = -80; EL_0 = -60;
I_0 = 24; I = @(t) I_0;       % constant external input current

% C_0 = [0.95, 1];
C_0 = [0.99, 1];
% C_0 = [1.01, 1];

% INITIAL CONDITIONS (specific ICs for the examples from the paper)
% x_0 = [-20 20 0.25 0.25 0.5 0.5]; % (a) IPS_1 [0.99, 1] and 0.04
% x_0 = [20 -20 0.25 0.25 0.5 0.5]; % (b) IPS_2 [1.01, 1] and 0.04
% x_0 = [-40 20 0.25 0.25 0.5 0.5]; % (c) APS [0.99 1] and 0.035
% x_0 = [-40 20 0.25 0.25 0.5 0.5]; % (d) quasi-APS [0.99 1] and 0.04
x_0 = [-30,0,0.05,0.05,0.05,0.5]; % (e) RS [0.99,1] and 0.04
% x_0 = [40,40,0.55,0.5,0.05,0.05]; % (f) quasiperiodic orbit [0.95, 1] and 0.04
% x_0 = [30,-35,0.01,0.1,0.4,0.5];  % (g) STO_1 [0.99,1] and 0.04
% x_0 = [-35,30,0.1,0.01,0.5,0.4];  % (h) STO_2 [0.99,1] and 0.04


% SIMULATION PARAMETERS
NN = 1;             % distinc parameter noise between neurons (N) or not (1)
shift = 0;          % transient period
sigma_noise = 0;    % std of input current noise
sigma_par = 0;      % std of parameters noise

params = struct('gNa',gNa_0,'gK',gK_0,'gL',gL_0,'ENa',ENa_0,...
    'EK',EK_0,'EL',EL_0,'I',I,'C',C_0);
data = struct('model','IN','params',params,'T',T,'dt',dt,'shift',shift,...
    'N',N,'K',K,'epsilon',epsilon,'sigma_par',sigma_par,...
    'sigma_noise',sigma_noise,'heter',(NN==N),'x_0',x_0);

% SIMULATION
[t,x,ie] = simInterneuron(K,data.params,x_0,data.T,data.dt,data.shift,data.sigma_noise);
if ~isempty(ie), error('The orbit diverged!'), end
data.x = x;

file = 'data_IN2_ex_RS'; newfile = file;
for i = 1:length(dir(Path))-2, if isfile([Path,newfile,'.mat']), newfile = [file,'_',num2str(i)]; else, break; end, end
% save([Path,newfile,'.mat'],'data');


% PLOTTING
% graphic stuff
xlims = [-45,37];
ylims = xlims;
ticks = xlims(1):15:xlims(2);
tlims = [0,50];
tticks = tlims(1):5:tlims(2);
lwd_orb = 0.5;

% data
X1 = data.x(:,1);
Y1 = data.x(:,2);
t = data.shift:data.dt:data.T-data.dt;
ind = t >= tlims(1) & t < tlims(2);
t1 = t(ind);

% plotting
fig = figure; tlo = tiledlayout(1,3);%'TileSpacing','compact');
set(fig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[9,3.25])
nexttile([1,1]), hold on, plot(X1(1:end),Y1(1:end),'b:','LineWidth',lwd_orb), 
plot(X1(end-1000:end),Y1(end-1000:end),'r-','LineWidth',1.5)
xlabel('V_1'), ylabel('V_2'),xlim(xlims), ylim(ylims), xtickangle(0)
set(gca,'XTick',ticks,'YTick',ticks,'box','on')

nexttile([1,2]), hold on, plot(t1,X1(ind),'b','LineWidth',lwd_orb), plot(t1,Y1(ind),'r','LineWidth',lwd_orb), hold off
xlabel('t'), ylabel('V_i'), xlim(tlims), ylim(ylims)
set(gca,'XTick',tticks,'YTick',ticks,'box','on')