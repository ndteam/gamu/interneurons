function [t,x,ie] = simInterneuron(K,params,x_0,T,dt,shift,sigma_noise)
%SIMINTERNEURON K, params, x_0, T, dt, shift, sigma_noise
%   Simulates coupled populations of N interneuron models.
%   K           gap-junctional coupling matrix (N x N non-negative matrix)
%   params      model parameters (structure)
%   x_0         initial conditions (vector of length 3*N)
%   T           time (length of the time interval)
%   dt          dt (time step)
%   shift       whole number between 0 and T
%   sigma_noise std of a white noise added to the signal during integration

if (shift < 0) || (shift > T)
    error('shift = %d must be between 0 and T = %d!',shift,T)
end

N_eq = length(K);
t = 0:dt:T;
gNa = params.gNa(:); gK = params.gK(:); gL = params.gL(:); C = params.C(:); 
ENa = params.ENa(:); EK = params.EK(:); EL = params.EL(:); I = params.I;

ie = [];
Opt = odeset('MaxStep',min(dt,5e-2),'RelTol',1e-3,'AbsTol',1e-6,'Events',@myEvent);
if sigma_noise == 0
    [t,x,~,~,ie] = ode45(@(t,x) interneuron(x,K,gNa,gK,gL,C,ENa,EK,EL,I(t)), t, x_0, Opt);
else
    % The scalar standard deviation of the noise is scaled by the vector C 
    % containing the capacitance of each neuron, turning this into a vector.     
    [t,x] = em(@(t,x) interneuron(x,K,gNa,gK,gL,C,ENa,EK,EL,I(t)), sigma_noise ./ sqrt(C), x_0, T, dt, N_eq);
end

x = x(t>shift,:);
t = t(t>shift);
if ~isempty(t), t = t-t(1); end
end

function [value, isterminal, direction] = myEvent(~,x)
value = (max(abs(x)) > 5000); % if the orbit diverges
isterminal = 1; % halt the integration
direction  = 0; % in any direction
end