function [t, X] = em(f, sigma_noise, X0, T, dt, N_eq)
% EM f, sigma_noise, X0, T, dt, N_eq
% Euler-Maruyama method for SDEs dX = f(X) dt + sigma dW where the noise is
% applied to the first N_eq equations (the first state variable of each neuron)
%   f             drift function
%   sigma_noise   noise coefficient (scaled by the capacitances)
%   X0            initial condition
%   T             total simulation time
%   dt            time step size
%   N_eq          number of cells/neurons

N = floor(T/dt);        % number of time steps
d = length(X0);         % dimension of the state space
sigma_noise = [sigma_noise; zeros(d-N_eq,1)];
X = zeros(d,N+1);
X(:,1) = X0;
t = 0:dt:T;
dW = [sqrt(dt) * randn(N_eq,N); zeros(d-N_eq,N)]; % increments of Wiener process

for n = 1:N
    X(:,n+1) = X(:,n) + f(t(n),X(:,n))*dt + sigma_noise .* dW(:,n);
end

X = X';     % rows to columns
end
