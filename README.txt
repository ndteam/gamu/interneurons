# Interneurons
Development and Support for modeling VHFOs and UFOs in EEG related to focal epilepsy.

This repository contains the necessary code for the execution of simulations that are included in the paper Cycle Multistability and Synchronization Mechanisms in Coupled Interneurons: In-Phase and Anti-Phase Dynamics under Current Stimuli. 

These subfolders include the following scripts and functions:
* IN (interneuron model)
    - script_00_IN_all_simulations. – smooth execution of all the following simulations;
    - script_01_IN_with_input_1*.m – diverse neuronal responses in the IN model for step current stimuli (Figs 1a–c);
    - script_02_IN2_examples_4*.m – examples of various dynamics in the model of two coupled INs (Figs 4a–h);
    - script_02_IN2_examples_A12.m – example of quasiperiodic dynamics in the model of two heterogeneous uncoupled INs (Fig A.12);
    - script_03_IN2_with_input_8*.m – diverse neuronal responses in the model of two coupled INs for step current stimuli (Figs 8a–f);
    - script_04_IN2_grid_sim_stoch_9.m – stochastic grid simulation of two coupled INs w.r.t. stimuli parameters (Figs 9, A.17a);
    - script_05_IN2x40_with_input_10.m – collective neuronal response of a large two-layer interneuronal network consisting of 80 coupled INs exposed to two distributed current stimuli, the emergence of transient collective anti-phase behavior leads to doubling of the dominant frequency in the summed signal (Fig 10);
    - script_06_IN2x40_grid_sim_11.m – stochastic grid simulation of a large two-layer interneuronal network consisting of 80 coupled INs w.r.t. stimuli parameters (Figs 11, A.17b);
    - script_07_IN2x40_with_input_A15 – collective neuronal response of a large two-layer interneuronal network consisting of 80 coupled INs exposed to two distributed current stimuli, the emergence of collective in-phase oscillations has no effect on the dominant frequency in the summed signal (Fig A.15);
    - script_08_IN2_grid_sim_det_A16.m – deterministic grid simulation of two coupled INs w.r.t. stimuli parameters (Fig A.16);
    - CoupledIN.txt – MatCont input used for bifurcation analysis of two coupled neurons, with parameters and equations (Figs 1d–e, 2, 3, 5–7, A.12, A.13, A.14). Can be adapted to 1 neuron and continuation w.r.t. other parameters.
* Other scripts/routines used for pre/postprocessing and running the simulations
    - interneuron.m – function determining the right-hand side of dynamical system of coupled INs;
    - getK.m – creates gap-junctional coupling matrix;
    - em.m – integration routine for Euler–Maruyama;
    - randtn.m – returns a truncated normal distribution for given parameters following Matlab documentation;
    - simInterneuron – simulates a corresponding neuronal network of coupled INs.
* data_IN_period.mat – period of a single IN model w.r.t. the externally applied DC current computed using MatCont toolbox.

Software versions used: MATLAB2023a and MatCont7p3

Copyright (c) 2025 Authors
