%% TWO ELECTRICALLY COUPLED INTERNEURONS EXPOSED TO AN EXTERNAL STIMULUS
% grid simulation w.r.t. the stimulus parameters

% SETTING
Path = [pwd, '\IN_sims\'];
clrs = ["b","r","g","k","c","y"];   % colors
per_clr = [0.9290,0.6940,0.1250];   % periodogram color

% TIME
T = 10+10+30;           % time span (shift+step+T)
dt = 0.01;

% COUPLING
n = 2;                  % number of neurons by cluster
k = length(n);          % number of clusters
N = sum(n);             % total number of neurons

% gap-junctional coupling
epsilon = 2*0.04;
K = getK(n,epsilon);
K = K/N;

% MODEL PARAMETERS
gNa_0 = 30; gK_0 = 20; gL_0 = 0.1; ENa_0 = 45; EK_0 = -80; EL_0 = -60;
C_0 = [0.99 1];

% INITIAL CONDITIONS (V,h,n,s)
x_0 = [ -79.91,-79.91,0.996,0.996,0.041,0.041 ]; % near the equilibrium state for I_ext = -2  

% SIMULATION PARAMETERS
NN = 1;             % distinc parameter noise between neurons (N) or not (1)
shift = 10;         % transient period
sigma_noise = 1;    % std of the input current noise
sigma_par = 0;      % std of parameters' multiplicative noise N(0,sigma_par^2)

input_range = [10,T];
step = 10;          % length of the subintervals

n_amplitudes = 401;
n_rhos = 401;
n_sims = 30;
n_steps = round((T-shift-step)/step);

amplitudes = linspace(12,29.5,n_amplitudes);
rhos = 1.1*linspace(-1,1,n_rhos);
F_comp_max = zeros(n_amplitudes,n_rhos,n_sims,n_steps);
F_sel_max = zeros(n_amplitudes,n_rhos,n_sims,n_steps);

load('data_IN_period.mat')
par = data.par;
period = data.period;
period_Iext = interp1(par,period,-2+amplitudes,'spline');

load([Path,'\data_IN2_grid_sim_stoch.mat'])
params = struct('gNa',gNa_0,'gK',gK_0,'gL',gL_0,'ENa',ENa_0,...
    'EK',EK_0,'EL',EL_0,'C',C_0);
data = struct('model','IN','params',params,'T',T,'dt',dt,'shift',shift,...
            'N',N,'K',K,'epsilon',epsilon,'sigma_par',sigma_par,...
            'sigma_noise',sigma_noise,'heter',(NN==N),'x_0',x_0,'input_range',input_range,...
            'amplitudes',amplitudes,'rhos',rhos,'step',step);


% SIMULATION
for i = 1:n_amplitudes
    deltas = period_Iext(i)*data.rhos;
    parfor j = 1:n_rhos
        for k = 21:30 % 1 = 50 min
            rng(1e6*i+1e3*j+k);             % for reproducibility
    
            % EXTERNAL CURRENT
            I_0 = -2;                       % baseline level of I_ext
            A_i = amplitudes(i)*[1;1];      % input amplitude
            
            delta = [0;deltas(j)];
            input_range_new = input_range+delta;
            
            % constant stimulus
            I = @(t) I_0 + A_i.*(t >= input_range_new(:,1) & t < input_range_new(:,2));
            params = data.params;
            params.I = I;
            
            % SIMULATION
            % periodograms are computed from one period after the stimulus onset for the last (second) neuron stimulated
            new_shift = data.shift+max(delta)+period_Iext(i); 
            [t,x,ie] = simInterneuron(K,params,x_0,data.T,data.dt,new_shift,data.sigma_noise);
            if ~isempty(ie), error('The orbit diverged!'), end
            
            for l = 1:n_steps
                from = (l-1)*step;
                ind = (t >= from) & (t < from+step);
    
                int_selected = [from,from+step];        % selected interval
                idx = (t >= int_selected(1)) & (t <= int_selected(2));
                x_sel = x(idx,1:2);
                
                [P_smoothed,f] = periodogram(detrend(x_sel),[],2^18,1000/dt);
                [P_i_max,P_i_max_ind] = max(P_smoothed(:,1));
                [~,P_max_ind] = max(P_i_max);
                ind_max = P_i_max_ind(P_max_ind);
                P_sel_smoothed = P_smoothed(:,P_max_ind);
                P_sel_max = P_sel_smoothed(ind_max);
                f_sel_max = f(ind_max); 
                
                % frequency of the composed signal
                composed_signal = sum(x(:,1:N),2); 
                [P_comp_smoothed,f_comp] = periodogram(detrend(composed_signal(idx)),[],2^18,1000/dt);
                [P_comp_max,P_comp_max_ind] = max(P_comp_smoothed);
                f_comp_max = f_comp(P_comp_max_ind);
    
                F_comp_max(i,j,k,l) = f_comp_max;
                F_sel_max(i,j,k,l) = f_sel_max;
            end
        end
    end
    i
end

data.F_comp_max = F_comp_max;
data.F_sel_max = F_sel_max;

file = 'data_IN2_grid_sim_stoch'; newfile = file;
for i = 1:length(dir(Path))-2, if isfile([Path,newfile,'.mat']), newfile = [file,'_',num2str(i)]; else, break; end, end
save([pwd,'\IN_sims\',newfile,'.mat'],'data');



%% PLOTTING
load([Path,'data_IN2_grid_sim_stoch.mat'])
grid_vals_1 = -2+data.amplitudes;
grid_vals_2 = data.rhos;
L = 3;      % number of 10-ms intervals to be accounted for

% DURATION OF THE APB
% M = data.F_comp_max(:,:,:,1:L) ./ data.F_sel_max(:,:,:,1:L); 
% M(isinf(M)) = 0; M = M > 1.5; M = data.step*sum(M,4); M = squeeze(mean(M,3,'omitnan')); 
% label = 'Total anti-phase duration [ms]'; c_ticks = linspace(0,10*L,6); clims = [0 10*L];

% OCCURRENCE OF THE APB
M = data.F_comp_max(:,:,:,1:L) ./ data.F_sel_max(:,:,:,1:L); 
M(isinf(M)) = 0; M = M(:,:,:,1:L) > 1.5; M = sum(M,4); M = M > 0; M = 100*squeeze(mean(M,3,'omitnan'));
label = 'Relative occurrence of anti-phase behavior [\%]'; c_ticks = 0:20:100; clims = [0 100];

% FREQUENCY OF THE COMPOSED SIGNAL
% M = data.F_comp_max(:,:,:,1:L); M = squeeze(mean(M,[3,4],'omitnan'));
% label = 'Mean dominant frequency [Hz]'; c_ticks = 200:100:700; clims = [200 600];

% M = data.F_comp_max(:,:,:,1:L); M = squeeze(max(M,[],[3,4],'omitnan'));
% label = 'Maximum dominant frequency [Hz]'; c_ticks = 250:150:850; clims = [225 850];

