%% TWO ELECTRICALLY COUPLED INTERNEURONS EXPOSED TO AN EXTERNAL STIMULUS (APS -> STO)
% diverse neuronal responses in the model of two coupled INs for step current stimuli

% SETTING
% clear, seed = 13;
% seed = seed+1;
% rng(seed) %seed 
Path = [pwd, '\IN_sims\'];
clrs = ["b","r","g","k","c","y"];   % colors
per_clr = [0.9290,0.6940,0.1250];   % periodogram color

% TIME
T = 150;                % time span
dt = 0.01;

% COUPLING
n = 2;                  % number of neurons by cluster
k = length(n);          % number of clusters
N = sum(n);             % total number of neurons

% gap-junctional coupling
epsilon = 2*0.04;
K = getK(n,epsilon);
K = K/N;

% MODEL PARAMETERS
gNa_0 = 30; gK_0 = 20; gL_0 = 0.1;
ENa_0 = 45; EK_0 = -80; EL_0 = -60;
C_0 = [0.99 1];

% EXTERNAL CURRENT
load('data_IN_period.mat')
par = data.par;
period = data.period;

% (a) RS -> IPS -> RS
% I_0 = -2;
% A_i = 25*[1;1];
% rho = 0.25;
% input_range = [50,100];


% (b) RS -> APS -> RS
% I_0 = -2;
% A_i = 25*[1;1];
% rho = 0.5;
% input_range = [50,100];


% (c) RS -> QAPS -> RS
% I_0 = -2;
% A_i = 26*[1;1];
% rho = 0.5;
% input_range = [50,T];


% (d) IPS -> APS -> IPS
% I_0 = 10;
% A_i = 13*[1;1];
% rho = 0.8;
% input_range = [50,100];


% (e) APS -> STO
I_0 = 23;
A_i = 4*[1;1];
rho = 0.8;
input_range = [51,100];


% (f) APS -> RS
% I_0 = 23;
% A_i = 4*[1;1];
% rho = 0.25;
% input_range = [50,100];


period_Iext = interp1(par,period,I_0+A_i(1),'spline');
delta = [0;rho*period_Iext];
input_range_new = input_range+delta;

% constant stimulus
I = @(t) I_0 + A_i.*(t >= input_range_new(:,1) & t < input_range_new(:,2));

% INITIAL CONDITIONS (V,h,n,s)
% x_0 = [ -79.91,-79.91,0.996,0.996,0.041,0.041 ]; % near the RS for I_ext = -2  
x_0 = [ 16.10,-40.63,0.1106,0.4429,0.5310,0.4394 ]; % near the APS for I_ext = 23
% x_0 = [-8.03,-0.07,0.0647,0.0759,0.5495,0.5487 ]; % near the ("top") IPS for I_ext = 10

% SIMULATION PARAMETERS
NN = 1;             % distinc parameter noise between neurons (N) or not (1)
shift = 0;          % transient period
sigma_noise = 0;    % std of the input current noise
sigma_par = 0;      % std of parameters' multiplicative noise N(0,sigma_par^2)

params = struct('gNa',gNa_0,'gK',gK_0,'gL',gL_0,'ENa',ENa_0,...
    'EK',EK_0,'EL',EL_0,'I',I,'C',C_0);
data = struct('model','IN','params',params,'T',T,'dt',dt,'shift',shift,...
    'N',N,'K',K,'epsilon',epsilon,'sigma_par',sigma_par,...
    'sigma_noise',sigma_noise,'heter',(NN==N),'x_0',x_0,'input_range',input_range);

[t,x,ie] = simInterneuron(K,data.params,x_0,data.T,data.dt,data.shift,data.sigma_noise);
if ~isempty(ie), error('The orbit diverged!'), end
data.x = x;
data.t = t;
data.input = I(t')';

file = 'data_IN2_APS_to_STO.mat';
for i = 1:length(dir(Path))-2, if isfile([Path,file]), file = ['data_',num2str(i),'.mat']; else, break; end, end
% save([pwd,'\IN_sims\',file],'data');

% PLOTTING
lwd = 0.25;
xlims = [0,T]; ylims = [-90,50]; ylims2 = [-2,39];

figure, tlo = tiledlayout(8,1,'TileSpacing','tight');
nexttile([3,1]), hold on, plot(data.t,data.x(:,1),'b','LineWidth',lwd), ylabel('V_i')
plot(data.t,data.x(:,2),'r','LineWidth',lwd), xlim(xlims), ylim(ylims), hold off, %set(gca,'box','off','Visible','off')
nexttile([3,1]), plot(data.t,data.x(:,1)+data.x(:,2),'b','LineWidth',lwd), ylabel('V_i')
xlim(xlims), ylim(2.*ylims),  %set(gca,'box','off','Visible','off')
nexttile([2,1]), hold on, plot(data.t,data.input(:,1),'b','LineWidth',lwd), xlim(xlims), ylim(ylims2), ylabel('I_{ext,i}'), xlabel('t')
plot(data.t,data.input(:,2),'r','LineWidth',lwd), xlim(xlims), ylim(ylims2), %set(gca,'box','off','Visible','off')
