%% DIVERSE NEURONAL RESPONSES TO STEP CURRENT STIMULI (SNIC)

% SETTING
% % clear, seed = 13;
% seed = seed+1;
% rng(seed)
Path = [pwd, '\IN_sims\'];
clrs = ["b","r","g","k","c","y"];   % colors
per_clr = [0.9290,0.6940,0.1250];   % periodogram color

% TIME
T = 200;                % time span
dt = 0.01;

% COUPLING
n = 1;                  % number of neurons by cluster
k = length(n);          % number of clusters
N = sum(n);             % total number of neurons

% gap-junctional coupling
epsilon = 0;
K = getK(n,epsilon);
K = K/N;

% MODEL PARAMETERS
gNa_0 = 30;
gK_0 = 20;
gL_0 = 0.1;
ENa_0 = 45;
EK_0 = -80;
EL_0 = -60;
C_0 = 1;

% EXTERNAL CURRENT
I_0 = -2;               % baseline level of I_ext
A = 10;                 % input amplitude
input_range = [50,70];
% input_range = [50,T];

% constant stimulus
I = @(t) I_0 + A.*(t >= input_range(1) & t <= input_range(2));

% INITIAL CONDITIONS (V,h,n,s)
x_0 = [ -80,0.99,0.04 ];          % near the RS for I_ext = -2
% x_0 = [ -16.24,0.0558,0.4302 ];   % near the RS for I_ext = 15
% x_0 = [ -42.5,0.414,0.456 ];      % near the LC for I_ext = 24


% SIMULATION PARAMETERS
NN = 1;             % distinc parameter noise between neurons (N) or not (1)
shift = 0;          % transient period
sigma_noise = 0;    % std of the input current noise
sigma_par = 0;      % std of parameters' multiplicative noise N(0,sigma_par^2)

params = struct('gNa',gNa_0,'gK',gK_0,'gL',gL_0,'ENa',ENa_0,...
    'EK',EK_0,'EL',EL_0,'I',I,'C',C_0);
data = struct('model','IN','params',params,'T',T,'dt',dt,'shift',shift,...
    'N',N,'K',K,'epsilon',epsilon,'sigma_par',sigma_par,...
    'sigma_noise',sigma_noise,'heter',(NN==N),'x_0',x_0,'input_range',input_range);

[t,x,ie] = simInterneuron(K,data.params,x_0,data.T,data.dt,data.shift,data.sigma_noise);
if ~isempty(ie), error('The orbit diverged!'), end
data.x = x;
data.t = t;
data.input = I(t);

file = 'data_SNIC_const_input'; newfile = file;
for i = 1:length(dir(Path))-2, if isfile([Path,newfile,'.mat']), newfile = [file,'_',num2str(i)]; else, break; end, end
% save([Path,newfile,'.mat'],'data');

lwd = 0.25;
xlims = [30,150]; ylims = [-90,50]; ylims1 = [-2,39]; ylims2 = [-2,39];

figure, tlo = tiledlayout(5,1,'TileSpacing','tight');
nexttile([3,1]), plot(data.t,data.x(:,1),'b','LineWidth',lwd), xlim(xlims), ylim(ylims) 
xlabel('t'), ylabel('V(t)'), %set(gca,'box','off','Visible','off')
nexttile([2,1]), plot(data.t,data.input,'b','LineWidth',lwd), xlim(xlims), ylim(ylims2)
xlabel('t'), ylabel('I_{ext}(t)'), %set(gca,'box','off','Visible','off')