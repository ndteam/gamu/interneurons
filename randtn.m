function [output] = randtn(mu,sigma,bounds,n)
%RANDTN mu, sigma, bounds, n
% Returns an array of pseudorandom numbers from trancated normal distribution.
%   mu      mean before truncation
%   sigma   standard deviation before truncation
%   bounds  interval bounds or the the one half of the inteval [mu-bounds, mu+bounds]
%   n       length or size of the 2D array

if length(n) == 1, n = [n,1]; end
if ((length(n) > 2) | (length(n) < 1)), error('The length of n must be 1 or 2!'), end

if length(bounds) == 1, bounds = [mu-bounds,mu+bounds]; end
if length(bounds) ~= 2, error('The attribute bounds must be a vector [a, b], a < b!'), end

untruncated = makedist('Normal',mu,sigma);
truncated = truncate(untruncated,bounds(1),bounds(2));

output = random(truncated,n(1),n(2));
end

