%% SCRIPT PROVIDING A SMOOTH EXECUTION OF ALL ATTACHED SIMULATIONS

%% FIGURE 1
script_01_IN_with_input_1a      % (a)
script_01_IN_with_input_1b      % (b)
script_01_IN_with_input_1c      % (c)

%% FIGURE 4
script_02_IN2_examples_4a       % (a)
script_02_IN2_examples_4b       % (b)
script_02_IN2_examples_4c       % (c)
script_02_IN2_examples_4d       % (d)
script_02_IN2_examples_4e       % (e)
script_02_IN2_examples_4f       % (f)
script_02_IN2_examples_4g       % (g)
script_02_IN2_examples_4h       % (h)

%% FIGURE 8
script_03_IN2_with_input_8a     % (a)
script_03_IN2_with_input_8b     % (b)
script_03_IN2_with_input_8c     % (c)
script_03_IN2_with_input_8d     % (d)
script_03_IN2_with_input_8e     % (e)
script_03_IN2_with_input_8f     % (f)

%% FIGURE 9
% script_04_IN2_grid_sim_9        % long-run simulation

%% FIGURE 10
script_05_IN2x40_with_input_10

%% FIGURE 11
% script_06_IN2x40_grid_sim_11    % long-run simulation

%% FIGURE A.14
script_07_IN2x40_with_input_A14
